import FluentProvider
import PostgreSQLProvider

extension Config {
    public func setup() throws {
        // allow fuzzy conversions for these types
        // (add your own types here)
        Node.fuzzy = [Row.self, JSON.self, Node.self]

        try setupProviders()
        try setupPreparations()
    }
    
    /// Configure providers
    private func setupProviders() throws {
        try addProvider(FluentProvider.Provider.self)
        
        
        do {
            try addProvider(PostgreSQLProvider.Provider.self)
        } catch {
            print("Error adding provider: \(error)")
        }
        
    }
    
    /// Add all models that should have their
    /// schemas prepared before the app boots
    private func setupPreparations() throws {
        //preparations.append(Post.self)
        preparations.append(voxus_user.self)
        preparations.append(Task.self)
    }
}
