import PostgreSQL
import PostgreSQLProvider
import Vapor

extension Droplet {
    func setupRoutes() throws {
        
        get("tasks"){ req in
            
            guard let tasks = try? Task.all().makeNode(in: nil) else{
                throw Abort.serverError
            }
            
            guard let response = try? Response(status: .accepted, json: JSON(node: tasks)) else{
                throw Abort.serverError
            }
            
            return response
        }
        
        post("task"){ req in
            
            guard let reqJson = req.json else{
                throw Abort.badRequest
            }
            
            guard let task = try? Task(json : reqJson) else{
                throw Abort.badRequest
            }
            
            guard (try? task.save()) != nil else{
                throw Abort.serverError
            }
            
            guard let response = try? Response(status: .accepted, json: JSON(node: req.json)) else{
                throw Abort.serverError
            }

            return response
        }
        
        put("task", Int.parameter){ req in
            let taskId = try req.parameters.next(Int.self)
            
            guard let task = try Task.find(taskId) else{
                throw Abort.badRequest
            }
            
            guard let taskJson = req.json else{
                throw Abort.badRequest
            }
            
            try task.update(json : taskJson)

            try task.save()
            
            let taskNode = try task.makeNode(in: nil)
            
            guard let response = try? Response(status: .accepted, json: JSON(node : taskNode)) else{
                throw Abort.serverError
            }
            
            return response
        }
        
        put("task", ":id/done"){ req in
            guard let taskId = req.parameters["id"]?.int else {
                throw Abort.badRequest
            }
            
            guard let task = try Task.find(taskId) else{
                throw Abort.badRequest
            }
            
            task.state = "done"
            
            try task.save()
            
            guard let response = try? Response(status: .accepted, json: JSON(arrayLiteral: ["mensagem" : "atualizado"])) else{
                throw Abort.serverError
            }
            
            return response
        }
        
        delete("task", Int.parameter){ req in
            let taskId = try req.parameters.next(Int.self)
            
            guard let task = try Task.find(taskId) else{
                throw Abort.badRequest
            }
            
            try task.delete()

            var responseJSON = JSON()
            try responseJSON.set("Message", ["Success" : "deletado"])
            
            guard let response = try? Response(status: .accepted, json: responseJSON) else{
                throw Abort.serverError
            }
            
            return response
        }
        
        post("login"){ req in
            
            guard let userJSON = req.json else{
                throw Abort.badRequest
            }
            
            let user = try voxus_user(json: userJSON)
            
            let returnedUser = try voxus_user.all().filter({ filterUser -> Bool in
                return filterUser.email == user.email
            })
            
            if returnedUser.isEmpty{
                try user.save()
                
                let userNode = try user.makeNode(in: nil)
                
                guard let response = try? Response(status: .accepted, json: JSON(node: userNode)) else{
                    throw Abort.serverError
                }
                
                return response
            }else{
                
                let user = returnedUser.first!
                
                let userNode = try user.makeNode(in: nil)
                
                guard let response = try? Response(status: .accepted, json: JSON(node: userNode)) else{
                    throw Abort.serverError
                }
                
                return response
            }
        }
    }
}
