//
//  Task.swift
//  back-end-voxusPackageDescription
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 21/11/17.
//

import Fluent
import FluentProvider
import Foundation
import PostgreSQLDriver
import Vapor

final class Task : Model{
    
    static let foreignKey = "user_id"
    
    var id : Identifier?
    var title : String
    var description : String
    var priority : Int
    var state : String
    var user_id : Int?
    let storage = Storage()

    func makeRow() throws -> Row {
        var row = Row()
        try row.set("id", id)
        try row.set("title", title)
        try row.set("description", description)
        try row.set("priority", priority)
        try row.set("state", state)
        try row.set("user_id", user_id)
        
        return row
    }
    
    init(row: Row) throws {
        self.id = try row.get("id")
        self.title = try row.get("title")
        self.description = try row.get("description")
        self.priority = try row.get("priority")
        self.state = try row.get("state")
        self.user_id = try row.get("user_id")
    }

    init(title : String, description : String, priority : Int, state : String, user_id : Int) {
        self.title = title
        self.description = description
        self.priority = priority
        self.state = state
        self.user_id = user_id
    }
    
    init(task : Task) {
        self.id = task.id
        self.title = task.title
        self.description = task.description
        self.priority = task.priority
        self.state = task.state
        self.user_id = task.user_id
    }
    
    func update(json : JSON) throws{
        self.title = try json.get("title")
        self.description = try json.get("description")
        self.priority = try json.get("priority")
        self.state = try json.get("state")
    }
}

extension Task : Preparation{

    static func prepare(_ database: Database) throws {}
    
    /// Undoes what was done in `prepare`
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}

extension Task : NodeRepresentable{
    func makeNode(in context: Context?) throws -> Node {
        var node = Node(context)
        
        try node.set("id", id)
        try node.set("title", title)
        try node.set("description", description)
        try node.set("priority", priority)
        try node.set("state", state)
        try node.set("user_id", user_id)
        
        return node
    }
}

extension Task : JSONInitializable{
    convenience init(json: JSON) throws {
        try self.init(title: json.get("title"), description: json.get("description"), priority: json.get("priority"), state : json.get("state"),user_id : json.get("user_id"))
    }
}
