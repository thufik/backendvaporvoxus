//
//  User.swift
//  back-end-voxusPackageDescription
//
//  Created by Fellipe Thufik Costa Gomes Hoashi on 27/12/17.
//

import Fluent
import FluentProvider
import Foundation
import PostgreSQLDriver
import Vapor

final class voxus_user: Model {
    let storage = Storage()
    
    var id : Identifier?
    var name : String
    var email : String

    init(id : Identifier? = nil, name : String, email : String) {
        self.id = id
        self.name = name
        self.email = email
    }
    
    func makeRow() throws -> Row {
        var row = Row()
        try row.set("id", id)
        try row.set("email", email)
        try row.set("name", name)

        
        return row
    }
    
    init(row: Row) throws {
        self.id = try row.get("id")
        self.name = try row.get("name")
        self.email = try row.get("email")
    }
    
    init(json : JSON) throws{
        self.id = try? json.get("id")
        self.name = try json.get("name")
        self.email = try json.get("email")
    }
}

extension voxus_user : Preparation{
    
    static func prepare(_ database: Database) throws {
    }
    
    /// Undoes what was done in `prepare`
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}

extension voxus_user : NodeRepresentable{
    func makeNode(in context: Context?) throws -> Node {
        var node = Node(context)
        
        try node.set("id", id)
        try node.set("name", name)
        try node.set("email", email)
        
        return node
    }
}
